\contentsline {section}{\numberline {1}Model }{3}
\contentsline {subsection}{\numberline {1.1}Basic Profile for User u}{3}
\contentsline {subsection}{\numberline {1.2}Serendipitous Profile for user u}{3}
\contentsline {subsection}{\numberline {1.3}Conjugate Gradient Algorithm for Dense Matrix}{5}
\contentsline {subsection}{\numberline {1.4}Conclusion}{6}
\contentsline {section}{\numberline {2}Preconditioned Conjugate Gradient Algorithm}{6}
\contentsline {subsection}{\numberline {2.1}Testing Result}{8}
\contentsline {subsection}{\numberline {2.2}Comparison with Conjugate Gradient Algorithm}{9}
\contentsline {subsection}{\numberline {2.3}Resource Analysis}{10}
\contentsline {subsection}{\numberline {2.4}Conclusion}{11}
\contentsline {section}{\numberline {3}LU Decomposition}{12}
\contentsline {subsection}{\numberline {3.1}Dense LU Decomposition Implementation on GPU}{12}
\contentsline {subsection}{\numberline {3.2}Sparse LU Decomposition Implementation on GPU}{13}
\contentsline {subsubsection}{\numberline {3.2.1}Sparse LU Decomposition Implementation on GPU for blocked matrix}{13}
\contentsline {subsubsection}{\numberline {3.2.2}General LU Decomposition}{14}
\contentsline {paragraph}{Symmetric LU Decomposition}{14}
\contentsline {paragraph}{Asymmetric LU Decomposition}{16}
\contentsline {subsection}{\numberline {3.3}Conclusion}{17}
\contentsline {section}{\numberline {4}Conclusion}{17}
